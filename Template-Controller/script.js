var test = angular.module('testApp', []);
test.directive('headerTitle', function () {
    return {
        restrict: 'E',
        templateUrl: 'header.html'
    };
});
test.directive('footerGan', function () {
    return {
        restrict: 'A',
        templateUrl: 'footer.html'
    };
});
test.directive('partTitle', function () {
    return {
        restrict: 'A',
        templateUrl: 'part.html',
        controller: function ($scope) {
            $scope.daftarNama = [{
                    nama: 'Souma',
                    kota: 'Tokyo'
                },
                {
                    nama: 'Nakiri',
                    kota: 'Shibuya'
                },
                {
                    nama: 'Joichiro',
                    kota: 'Nihongo'
                }
            ];
        }
    };
});