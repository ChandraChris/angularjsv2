var test = angular.module('testApp', ['ngRoute']);
test.directive('headerTitle', function () {
    return {
        restrict: 'E',
        templateUrl: 'header.html'
    };
});
test.directive('footerGan', function () {
    return {
        restrict: 'A',
        templateUrl: 'footer.html'
    };
});
test.directive('partTitle', function () {
    return {
        restrict: 'A',
        templateUrl: 'part.html',
        controller: function ($scope) {
            $scope.daftarNama = [{
                    nama: 'Souma',
                    kota: 'Tokyo'
                },
                {
                    nama: 'Nakiri',
                    kota: 'Shibuya'
                },
                {
                    nama: 'Joichiro',
                    kota: 'Nihongo'
                }
            ];
        }
    };
});
test.config(function ($routeProvider) {
    $routeProvider
        .when('/tambah-info', {
            templateUrl: 'index2.html',
            controller: 'add'
        })
        .when('/kontak', {
            templateUrl: 'kontak.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});
test.controller('add', function ($scope) {
    $scope.tambahData = function () {
        $scope.daftarNama.push({
            nama: $scope.databaru.nama,
            kota: $scope.databaru.kota
        });
    }
});