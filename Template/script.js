var test = angular.module('testApp', []);
test.controller('dataOrang', function ($scope) {
    $scope.daftarNama = [{
            nama: 'Souma',
            kota: 'Tokyo'
        },
        {
            nama: 'Nakiri',
            kota: 'Shibuya'
        },
        {
            nama: 'Joichiro',
            kota: 'Nihongo'
        }
    ];
});
test.directive('headerTitle', function () {
    return {
        restrict: 'E',
        templateUrl: 'header.html'
    };
});
test.directive('footerGan', function () {
    return {
        restrict: 'A',
        templateUrl: 'footer.html'
    };
});