var test = angular.module('testApp', ['ngRoute']);
test.factory('factoryData', function ($http) {
    var factoryOrang = {};

    factoryOrang.getOrang = function () {
        return $http.get('data.php');
    }
    return factoryOrang;
});
test.directive('headerTitle', function () {
    return {
        restrict: 'E',
        templateUrl: 'header.html'
    };
});
test.directive('footerGan', function () {
    return {
        restrict: 'A',
        templateUrl: 'footer.html'
    };
});
test.directive('partTitle', function () {
    return {
        restrict: 'A',
        templateUrl: 'part.html',
        controller: function ($scope, factoryData) {
            factoryData.getOrang().then(function (datas) {
                $scope.daftarNama = datas.data;
            })
        }
    };
});
test.config(function ($routeProvider) {
    $routeProvider
        .when('/tambah-info', {
            templateUrl: 'index2.html',
            controller: 'add'
        })
        .when('/kontak', {
            templateUrl: 'kontak.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});
test.controller('add', function ($scope) {
    $scope.tambahData = function () {
        $scope.daftarNama.push({
            nama: $scope.databaru.nama,
            kota: $scope.databaru.kota
        });
    }
});